<?php

/**
 * Captcha config file
 */
$captchaConfig = [
    //'new_code_text' => 'Get new code',
    //'code_algorithm' => 'math', //algorithm for code generation, 'random'|'math'
    //'render_algorithm' => 'char',  //algorithm for rendering, as single string or individual chars, 'string'|'char'

    //'char_font_space' => 3,          //font space, applies only for render_algorithm char
    //'session_key' => 'captcha_code', //name of key in session and in form
    //'length' => 5,                   //length of captcha in chars
    //'width' => 220,                  //image width in pixel
    //'height' => 35,                  //image height in pixel

    //'bg_color' => array('r' => 240, 'g' => 240, 'b' => 240), //pixel color RGB color sheme or 'random'
    //'bg_color' => array('r' => 240, 'g' => 240, 'b' => 240), //pixel color RGB color sheme or 'random'
    //'bg_random_color_range' => array('min' => 200, 'max' => 255),

    //'v_line_count' => 5,               //count of vertical lines to add to image, set to 0 for no lines
    //'v_line_width' => 2,               //vertical line width
    //'v_line_color' => array('r' => 64, 'g' => 64, 'b' => 64), //line color RGB color sheme or 'random'
    //'v_line_random_color_range' => array('min' => 0, 'max' => 140),

    //'h_line_count' => 3,               //count of horizontal lines to add to image, set to 0 for no lines
    //'h_line_width' => 2,               //horizontal line width
    //'h_line_color' => array('r' => 64, 'g' => 64, 'b' => 64), //line color RGB color sheme or 'random'
    //'h_line_random_color_range' => array('min' => 0, 'max' => 140),
    //'h_line_algorithm' => 'straight',  //algorithm for horizontal lines, possible values 'straight'|'sinus'

    //'pixel_count' => 400,            //count of pixel to add to image, set to 0 for no pixel
    //'pixel_color' => array('r' => 0, 'g' => 0, 'b' => 255), //pixel color RGB color sheme or 'random'
    //'pixel_random_color_range' => array('min' => 0, 'max' => 140),

    //change font, some fonts automatic vertical positioning not work correctly, but then you can work with 'code_position' => array('x' => 0, 'y' => 20)
    //possible values are 'default' which takes the first entry from the fonts array, 'random' which choose one randomly or an integer
    //that chooses the entry with that index or manual setting a value between 0 for first font and 3 for last font of font list
    //'font' => 'default',
    //'font_size' => 14,           //font size of code, possible values are 'auto'| integer number 10, 21, 33 ...

    //'code_position' => 'auto',       //automatic position center vertical and horizontal
    //'code_position' => array('x' => 5, 'y' => 30), //choose start position manually by x/y coords
    //'auto_max_width' => 0.5,         //how much of the image space the code is allowed to use
    //'code_color' => array('r' => 64, 'g' => 64, 'b' => 64), //code color RGB color sheme or 'random'
    //'code_random_color_range' => array('min' => 0, 'max' => 140),
    /*digit translations for mathematical captcha
    'digits' => [
        '0'	=> 'null',
        '1' => 'eins',
        '2' => 'zwei',
        '3' => 'drei',
        '4' => 'vier',
        '5' => 'fünf',
        '6' => 'sechs',
        '7' => 'sieben',
        '8' => 'acht',
        '9' => 'neun'
    ],
    */

    /*operator translations for mathematical captcha
    'operators' => [
        '+'	=> 'plus',
        '-' => 'minus',
        '*' => 'mal'
    ],
    */
];
