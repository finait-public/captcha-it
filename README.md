# Captcha-it

Captcha-it allows to create captcha images without cookies. So no DSGVO issues.
Almost all aspects like size, colors, lines, pixels and fonts are configurable.

## Getting Started

The project comes with examples how to use Captcha-it. The file index.php contains an
example form and an example how to process the form.

The file submit.php contains an example for ajax form processing. The JavaScript code
can be found in assets/scripts.js.

The form image is displayed by using the file captcha/captcha_image.php in the src
attribute of the image tag.

### Prerequisites

1. Webserver with PHP 7.1 +
2. PHP GD extension

### Installing

Just copy it to a subfolder of your project. Remove the files you don't need, e.g index.php or submit.php.
They only contain examples. Use Captcha-it like shown in the examples.

## Deployment

Deploy it with your regular project.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/finait-public/captcha-it/tags). 

## Authors

* *Damir Abdijevic* - *Initial work* - [Damir Abdijevic](https://gitlab.com/finait-public/captcha-it)
* *FinaIT®* - *Initial work* - [FinaIT®](https://finait.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
The fonts contained are all from the public domain. The project contains all original zip files with
the original license from the authors. To that font files the original license of the authors applies.

## Usage

Captcha-it ships with default values for every aspect of image generation. But all settings can be overriden
in the file captcha_config.php. So you don't have to modify the original class Captcha.php. For every setting
something is written in captcha_config.php.

### Features

* You can generate a random string including numbers or a mathematical calculation, that is used as a captcha image.
* You can render it as a single string or as individual chars that have some random vertical offset.
* You can adjust the color of the background, the lines drawn, the pixel added to the image and the color of the captcha code itself. 
* Instead of setting the color manually you can choose random values. For random values you can specify a random range.
* Lines can be drawn vertically and or horizontally and the count set. Horizontal lines can be drawn as straight lines or a sinus curve. 
* The captcha code can be positioned manually or automatic centered horizontally and vertically
* You can set the font size manually or let it choose automatically. It will increase so long until a given percentage of the image is reached.
This is only implemented for render algorithm string.

### Examples

All adjustments refer to the file captcha_config.php. Just remove the comments for a setting and adjust it to
a desired value.

#### Use random string, with automatic position rendered as individual chars:
```PHP
$captchaConfig = [
//...
    'render_algorithm' => 'char'
//...
];
```
#### Change font, possible values: default|random|0|1|2|3:
```PHP
$captchaConfig = [
//...
    'font' => 1
//...
];
```
#### Generate mathematical calculation with automatic positioning rendered as string:
```PHP
$captchaConfig = [
//...
    'code_algorithm' => 'math',
    'render_algorithm' => 'string',
    'width' => 220,                  
    'height' => 35,
    'font_size' => 14
//...
];
```
#### Generate mathematical calculation with automatic positioning rendered as individual chars:
```PHP
$captchaConfig = [
//...
    'code_algorithm' => 'char',
    'render_algorithm' => 'string',
    'width' => 220,                  
    'height' => 35,
    'font_size' => 14
//...
];
```
#### Add 2 sinus curves with width of 3 with random color within a given color range:
```PHP
$captchaConfig = [
//...
    'h_line_count' => 2,
    'h_line_width' => 3,
    'h_line_color' => 'random',
    'h_line_random_color_range' => array('min' => 0, 'max' => 140),
    'h_line_algorithm' => 'sinus' 
//...
];
```
#### Add 600 pixel in random color within a given color range and draw random chars in random color within given color range:
```PHP
$captchaConfig = [
//...
    'pixel_count' => 600,
    'pixel_color' => 'random',
    'pixel_random_color_range' => array('min' => 80, 'max' => 170),
    'code_color' => 'random',
    'code_random_color_range' => array('min' => 0, 'max' => 140)
//...
];
```
#### Draw mathematical captcha but allow only plus and minus as operators:
```PHP
$captchaConfig = [
//...
    'code_algorithm' => 'math', 
    'width' => 180, 
    'font_size' => 13,
    'operators' => [
        '+' => 'plus',
        '-' => 'minus'
    ]   
//...
];
```