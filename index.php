<?php
session_start();
require_once('./Captcha.php');
require_once "./captcha_config.php";

$captcha = new Captcha($captchaConfig);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $captchaCode = filter_var($_POST[$captcha->getConfValue('session_key')],FILTER_SANITIZE_STRING, FILTER_REQUIRE_SCALAR);
    $isValid = $captcha->isValid($captchaCode);
}

?>
<html lang="de">
<head>
  <meta charset="utf-8">
  <title>Captcha Dev</title>
    <script src="/assets/jquery-3.4.1.min.js"></script>
    <script src="/assets/scripts.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/styles.css">
</head>

<body>
    <div class="form_errors"></div>
	<form id="ajax_form" action="submit.php" method="post">
	<div class="form-row">
		<label for="first_name">Vorname:</label><input type="text" name="first_name" id="first_name">
	</div>
    <div class="form-row">
		<label for="last_name">Nachname:</label><input type="text" name="last_name" id="last_name">
	</div>
    <div class="form-row captcha">
        <img id="captcha_image" src="/captcha_image.php">
        <?php if ($captcha->getConfValue('new_code_text')) :?>
            <a id="captcha_new_image" href="#"><?php echo $captcha->getConfValue('new_code_text')?></a>
        <?php endif;?>
        <input id="captcha_code" placeholder="Verificationcode*" name="<?php echo $captcha->getConfValue('session_key')?>" id="<?php echo $captcha->getConfValue('session_key')?>" type="text" class="captcha-input">
	</div>
	<div class="form-row">
		<input type="submit" value="absenden" id="submit_form">
	</div>
	</form>
</body>
</html>