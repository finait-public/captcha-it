<?php
session_start();

require_once "./Captcha.php";
require_once "./captcha_config.php";

$captcha = new Captcha($captchaConfig);
$imageData = $captcha->generate();
$captcha->render($imageData);