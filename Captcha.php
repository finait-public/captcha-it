<?php

class Captcha
{
    /**
     * @var array
     */
    private $conf = [
        'new_code_text' => 'get new code',
        'code_algorithm' => 'random_chars',
        'render_algorithm' => 'string',
        'char_font_space' => 2,
        'session_key' => 'captcha_code',
        'length' => 5,
        'width' => 120,
        'height' => 35,
        'bg_color' => ['r' => 200, 'g' => 200, 'b' => 200],
        'bg_random_color_range' => ['min' => 150, 'max' => 255],
        'v_line_count' => 0,
        'v_line_width' => 1,
        'v_line_color' => ['r' => 64, 'g' => 64, 'b' => 64],
        'v_line_random_color_range' => ['min' => 0, 'max' => 140],
        'h_line_count' => 2,
        'h_line_width' => 2,
        'h_line_color' => ['r' => 64, 'g' => 64, 'b' => 64],
        'h_line_random_color_range' => ['min' => 0, 'max' => 140],
        'h_line_algorithm' => 'sinus',
        'pixel_count' => 400,
        'pixel_color' => ['r' => 0, 'g' => 0, 'b' => 255],
        'pixel_random_color_range' => ['min' => 0, 'max' => 255],
        'font_list' => [
            __DIR__ . '/fonts/Overhaul.ttf',
            __DIR__ . '/fonts/Capture it.ttf',
            __DIR__ . '/fonts/Carbtim.ttf',
            __DIR__ . '/fonts/PhatGrunge.ttf'
        ],
        'font' => 'default',
        'font_size' => 16,
        'code_position' => 'auto',
        'auto_max_width' => 0.8,
        'code_color' => ['r' => 0, 'g' => 0, 'b' => 0],
        'code_random_color_range' => ['min' => 0, 'max' => 140],
        'digits' => [
            '0'	=> 'null',
            '1' => 'eins',
            '2' => 'zwei',
            '3' => 'drei',
            '4' => 'vier',
            '5' => 'fünf',
            '6' => 'sechs',
            '7' => 'sieben',
            '8' => 'acht',
            '9' => 'neun'
        ],
        'operators' => [
            '+'	=> 'plus',
            '-' => 'minus',
            '*' => 'mal'
        ]
    ];

    private $img;

    /**
     * Captcha constructor.
     *
     * @param array $conf Configuration data
     */
    public function __construct(array $conf = []) {
        $this->conf = array_merge($this->conf, $conf);
    }

    /**
     * Generate the captcha image
     * 
     * @return false|resource
     *
     * @throws Exception
     */
    public function generate()
    {
        switch ($this->conf['code_algorithm']) {
            case 'random_chars':
                $code = $this->getCodeChars();
                break;
            case 'math':
                $code = $this->getCalculation();
                break;
            default:
                throw new InvalidArgumentException('Invalid code algorithm: ' . $this->conf['code_algorithm']);
        }

        $this->setSession($this->conf['session_key'], $code['result']);

        return $this->createImage($code['code']);
    }

    /**
     * Get the chars for captcha code
     *
     * @return array
     *
     * @throws Exception
     */
    private function getCodeChars() :array
    {
        $code = $result = strtoupper(substr(md5(random_bytes(64)), 0, $this->conf['length']));
        return [
            'code' => $code,
            'result' => $result
        ];
    }

    /**
     * Get math exercise
     *
     * @return array
     */
    private function getCalculation() :array
    {
        $c = $this->conf;
        $firstOperandDigit  =  rand(0, count($c['digits']) -1);
        $secondOperandDigit =  rand(0, count($c['digits']) -1);

        $firstOperandWord  = $c['digits'][$firstOperandDigit];
        $secondOperandWord = $c['digits'][$secondOperandDigit];

        $operatorKey = rand(0, count($c['operators']) -1);
        $operatorSign = array_keys($c['operators'])[$operatorKey];
        $operatorWord  = array_values($c['operators'])[$operatorKey];

        switch($operatorSign) {
            case '+';
                $result = $firstOperandDigit + $secondOperandDigit;
                break;
            case '-';
                $result = $firstOperandDigit - $secondOperandDigit;
                break;
            case '*';
                $result = $firstOperandDigit * $secondOperandDigit;
                break;
            default:
                throw new InvalidArgumentException('Operator not supported: ' . $operatorSign);
        }

        return [
            'code'   => $firstOperandWord . ' ' . $operatorWord . ' ' . $secondOperandWord,
            'result' => $result
        ];
    }

    /**
     * Set session data
     *
     * @param string $key   Session key
     * @param string $value Session value
     *
     * @return void
     */
    private function setSession(string $key, string $value) :void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Get session data
     * 
     * @param string $key Session key
     *
     * @return array
     */
    public function getSession(string $key) :string
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] . ''  : '';
    }

    /**
     * Get a config value
     *
     * @param string $key Config key
     *
     * @return mixed|null
     */
    public function getConfValue(string $key)
    {
        return isset($this->conf[$key]) ? $this->conf[$key] : null;
    }

    /**
     * Create the captcha base image
     *
     * @param string $code Captcha code
     *
     * @return false|resource
     */
    private function createImage(string $code)
    {
        $c = $this->conf;
        $this->img = imagecreatetruecolor($c['width'], $c['height']);
        $this->addBackground();
        if ($c['v_line_count'] > 0) {
            $this->addLines('v');
        }
        if ($c['h_line_count'] > 0) {
            switch($c['h_line_algorithm']) {
                case 'straight':
                    $this->addLines('h');
                    break;
                case 'sinus':
                    $this->addSinusCurves();
                    break;
                default:
                    throw new InvalidArgumentException('Invalid line algorithm: ' . $c['h_line_algorithm']);
            }
        }
        if ($c['pixel_count'] > 0) {
            $this->addPixel();
        }

        $this->addCode($code);

        return $this->img;
    }

    /**
     * Add background to image
     *
     * @return void
     */
    private function addBackground() :void
    {
        $background = imagecolorallocate(
            $this->img,
            $this->getColor('bg', 'r'),
            $this->getColor('bg', 'g'),
            $this->getColor('bg', 'b'));

        imagefill($this->img,0,0, $background);
    }

    /**
     * Add vertical lines to image
     *
     * @param string $prefix Config key prefix
     *
     * @return void
     */
    private function addLines(string $prefix) :void
    {
        $c = $this->conf;
        $lineColor = imagecolorallocate(
            $this->img,
            $this->getColor($prefix . '_line', 'r'),
            $this->getColor($prefix . '_line', 'g'),
            $this->getColor($prefix . '_line', 'b')
        );
        $isVertical = $prefix == 'v';
        for($i = 0; $i < $c[$prefix . '_line_count']; $i++) {
            if ($isVertical) {
                $x1 = $i * intval($c['width'] / $c[$prefix . '_line_count']);
                $y1 = 0;
                $x2 = rand(0, $c['width']);
                $y2 = $c['height'];
            } else {
                $x1 = 0;
                $y1 = $i * intval($c['height'] / $c[$prefix . '_line_count']);
                $x2 = $c['width'];
                $y2 = rand(0, $c['height']);
            }

            for ($j = 0; $j < $c[$prefix . '_line_width']; $j++) {
                imageline(
                    $this->img,
                    $x1 + ($isVertical ? $j : 0),
                    $y1 + ($isVertical ? 0 : $j),
                    $x2 + ($isVertical ? $j : 0),
                    $y2 + ($isVertical ? 0 : $j),
                    $lineColor
                );
            }
        }
    }

    /**
     * Add sinus curves to image
     *
     * @return void
     */
    private function addSinusCurves() :void
    {
        $c = $this->conf;
        $lineColor = imagecolorallocate(
            $this->img,
            $this->getColor('h_line', 'r'),
            $this->getColor('h_line', 'g'),
            $this->getColor('h_line', 'b')
        );

        for($i = 0; $i < $c['h_line_count']; $i++) {
            $y = 0;
            $degradOffset = rand(-200, 200);
            for ($x = 0; $x < $c['width']; ++$x) {
                $amplitude = $c['height'] / 2.2 ;
                $y = $c['height'] / 2 + $amplitude * sin(deg2rad($x + $degradOffset));

                for ($j = 0; $j < $c['h_line_width']; $j++) {
                    imagesetpixel($this->img, $x, $y + $j, $lineColor);
                }

            }
        }
    }

    /**
     * Add some random pixel to image
     *
     * @return void
     */
    private function addPixel() :void
    {
        $c = $this->conf;
        $pixelColor = imagecolorallocate(
            $this->img,
            $this->getColor('pixel', 'r'),
            $this->getColor('pixel', 'g'),
            $this->getColor('pixel', 'b')
        );

        for($i = 0; $i < $c['pixel_count']; $i++) {
            imagesetpixel($this->img, rand(0, $c['width']), rand(0, $c['height']), $pixelColor);
        }
    }

    /**
     * Add the captcha code to the image
     *
     * @param string $code Captcha code
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    private function addCode(string $code) :void
    {
        switch ($this->conf['render_algorithm']) {
            case 'string':
                $this->drawString($code);
                break;
            case 'char' :
                $this->drawChars($code);
                break;
            default:
                throw new InvalidArgumentException(
                    'Invalid render algorithm: ' . $this->conf['render_algorithm']
                );
        }
    }

    /**
     * Draw captcha code as single string
     *
     * @param string $code Captcha code
     *
     * @return void
     */
    private function drawString(string $code) :void
    {
        $c = $this->conf;
        $fontSize = $c['font_size'];
        $imgWidth = $c['width'];
        $imgHeight = $c['height'];
        $font = $this->getFont();

        $codeColor = imagecolorallocate(
            $this->img,
            $this->getColor('code', 'r'),
            $this->getColor('code', 'g'),
            $this->getColor('code', 'b')
        );

        if ($fontSize == 'auto') {
            $fontSize = 1;
            $txtMaxWidth = intval($c['auto_max_width'] * $imgWidth);
            do {
                $fontSize++;
                $p = imagettfbbox($fontSize, 0, $font, $code);
                $txtWidth = $p[2] - $p[0];
                $txtHeight = $p[1] - $p[7];
            } while ($txtWidth <= $txtMaxWidth);
        } else {
            $p = imagettfbbox($fontSize, 0, $font, $code);
            $txtWidth = $p[2] - $p[0];
            $txtHeight =$p[1] -$p[7];
        }

        if ($c['code_position'] == 'auto') {
            $x = ($imgWidth - $txtWidth) / 2;
            $y = $txtHeight + (($imgHeight - $txtHeight) / 2);
        } else {
            $x = $c['code_position']['x'];
            $y = $c['code_position']['y'];
        }

        imagettftext($this->img, $fontSize, 0, $x, $y, $codeColor, $font, $code);
    }

    /**
     * Draw captcha code as separate chars
     *
     * @param string $code Captcha code
     *
     * @return void
     */
    private function drawChars(string $code) :void
    {
        $c = $this->conf;
        $imgWidth = $c['width'];
        $imgHeight = $c['height'];
        $fontSize = $c['font_size'];
        $x = 0;
        $startY = $fontSize + 9;

        if (! is_numeric($fontSize)) {
            throw new InvalidArgumentException('Only numeric font size is implemented for algorithm char yet.');
        }

        $imgLayer = imagecreatetruecolor($c['width'], $c['height']);

        imagealphablending($imgLayer, false);
        $transparency = imagecolorallocatealpha($imgLayer, 0, 0, 0, 127);
        imagefill($imgLayer, 0, 0, $transparency);
        imagesavealpha($imgLayer, true);

        $maxY = 0;
        $minY = $startY;

        for ($i = 0; $i < strlen($code); $i++) {
            $font = $this->getFont();
            $char = substr($code, $i, 1);

            $col = imagecolorallocate(
                $imgLayer,
                $this->getColor('code', 'r'),
                $this->getColor('code', 'g'),
                $this->getColor('code', 'b')
            );

            $angle = rand(0, 25);

            $y = $startY + rand(0, 7);

            if ($x == 0) {
                $x += $fontSize * (($angle * 1.25) / 100);
            }
            imagettftext($imgLayer, $fontSize, $angle, $x, $y, $col, $font, $char);

            $dim = imagettfbbox($fontSize, $angle, $font, $char);

            $x = $this->calcNewXPos($x, $dim);
            $yTextTop = $y - ($dim[1] - $dim[7]);

            $maxY = ($y > $maxY) ? $y : $maxY;
            $minY = ($yTextTop < $minY) ? $yTextTop : $minY;
        }

        $maxY = ($maxY > $imgHeight) ? $imgHeight : $maxY;
        $minY = ($minY < 0) ? 0 : $minY;

        $txtImageWidth = $x - $this->conf['char_font_space'];
        $txtImageHeight = $maxY - $minY;

        if ($c['code_position'] === 'auto') {
            $x = ($imgWidth - $txtImageWidth) / 2;
            $y = ($imgHeight - $txtImageHeight) / 2;
        } else {
            $x = $c['code_position']['x'];
            $y = $c['code_position']['y'] - $txtImageHeight;
        }

        imagecopy($this->img, $imgLayer, $x, $y, 0, $minY, $txtImageWidth, $txtImageHeight);
    }

    /**
     * Calculate new x position
     *
     * @param $xPos Current x position
     * @param $dim  Image dimension
     *
     * @return float|int|mixed
     */
    protected function calcNewXPos($xPos, $dim)
    {
        $fontSpace = $this->conf['char_font_space'];
        $xPos += $dim[4] + abs($dim[6]) + $fontSpace;
        return $xPos;
    }

    /**
     * Return font path
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function getFont() :string
    {
        $c = $this->conf;
        $font = $c['font'];
        if (is_numeric($font)) {
            return $c['font_list'][$font];
        } else {
            if ($font == 'default') {
                return $c['font_list'][0];
            }
            if ($font == 'random') {
                return $c['font_list'][rand(0, count($c['font_list']) - 1)];
            }
        }

        throw new InvalidArgumentException('Invalid font specified ' . $font);
    }

    /**
     * Get rgb color value for a given purpose
     *
     * @param string $type Purpose of color e.g. code|v_line|pixel
     * @param string $rgb  Which color value of rgb scheme e.g. r|g|b
     *
     * @return int
     */
    private function getColor(string $type, string $rgb) :int
    {
        $typeRgb = $this->conf[$type . '_color'];
        if ($typeRgb == 'random') {
            $typeRandomRange = $this->conf[$type . '_random_color_range'];
            return rand($typeRandomRange['min'], $typeRandomRange['max']);
        } else {
            return $typeRgb[$rgb];
        }
    }

    /**
     * Check if given captcha code is valid
     *
     * @param string $formCaptcha Submitted captcha code from form
     *
     * @return bool
     */
    public function isValid(string $formCaptcha) :bool
    {
        $sessionCaptcha = $this->getSession($this->conf['session_key']);

        return $sessionCaptcha == $formCaptcha;
    }

    /**
     * Render captcha image
     *
     * @param resource $image Image resource to render
     *
     * @return void
     */
    public function render($image) :void
    {
        header("Content-type: image/jpeg");
        imagejpeg($image, null, 100);
        imagedestroy($image);
    }
}

