<?php
session_start();
require_once('./Captcha.php');
require_once "./captcha_config.php";

$captcha = new Captcha($captchaConfig);
$isValid = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $captchaCode = filter_var($_POST[$captcha->getConfValue('session_key')],FILTER_SANITIZE_STRING, FILTER_REQUIRE_SCALAR);
    $isValid = $captcha->isValid($captchaCode);
}

if ($isValid) {
    $response = ['result' => 'success'];
} else {
    $response = ['result' => 'error', 'message' => 'invlid captcha'];
    header("HTTP/1.0 400 Bad Request");
}
echo json_encode($response);

exit;