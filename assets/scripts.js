$( document ).ready(function() {

    function reloadImage() {
        $("#captcha_image").attr('src', '/captcha_image.php?' +  new Date().getTime());
    }

    $('#captcha_new_image').click(function(e) {
        e.preventDefault();
        reloadImage();
    })

    $('#ajax_form').submit(function(e) {
        e.preventDefault();
        $('.form_errors').empty();
        $('#captcha_code').removeClass('error');

        // Send form using ajax
        var form = $(this);
        $.ajax({
            type: 'POST',
            url: form.prop('action'),
            data : form.serialize(),
            dataType: 'json',
            encode: true,
            statusCode : {
                400: function(data, textStatus, jqXHR) {
                    if (data.responseJSON.message == 'invlid captcha') {
                        $('#captcha_code').addClass('error');
                        reloadImage();
                        $('.form_errors')
                            .append($('<div>Captcha Eingabe falsch. Bitte versuchen Sie es erneut.</div>').addClass("validation-error"));
                    }
                }
            }
        }).done(function(data) {
            // Aktionen bei Erfolg
            console.log('done: '+data);
        }).fail(function(data) {
            // Aktionen bei einem Fehler
            console.log('fail: '+data);
        });
    })
});

